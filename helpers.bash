#!/bin/bash

USAGE='my_echo -t [-c] <title> -c <1|2|3|4|5|6|7> <message>'

function my_echo {
	until [ -z "$1" ]; 
	do
		case $1 in
			--help|-h )
				shift
				echo $USAGE
				return 1
				;;
			--color|-c )
				shift
				# TODO: check for failure (bad color)
				tput setaf $1
				;;
			--title|-t )
				shift
				tput smul
				tput bold
				printf "[$1]`tput rmul`: "
				tput sgr0 # color gets cleared after title printed
				;;
			-* )
				echo "Unrecognized option $1" && return 1
				;;
			* )
				printf "$1 \n"
				tput sgr0
				return 0
				;;
		esac
		shift
		if [ "$#" = "0" ];
			then
			break;
		fi
	done
}

function prompt {
	echo "Press Enter to continue. Ctrl-C to quit"
	read -n 1
}
